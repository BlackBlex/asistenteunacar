﻿using FullSerializer;
using IBM.Watson.DeveloperCloud.Connection;
using IBM.Watson.DeveloperCloud.DataTypes;
using IBM.Watson.DeveloperCloud.Logging;
using IBM.Watson.DeveloperCloud.Services.Assistant.v1;
using IBM.Watson.DeveloperCloud.Services.SpeechToText.v1;
using IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1;
using IBM.Watson.DeveloperCloud.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Networking;
using SimpleJSON;
using System.Text.RegularExpressions;

public class ItemLink
{
    public string Imagen { get; set; }
    public string Link { get; set; }
    public string Title { get; set; }

    public ItemLink(string link, string title, string img)
    {
        Imagen = img;
        Link = link;
        Title = title;
    }

    public override string ToString()
    {
        return "Imagen: " + Imagen + " Link: " + Link + " Title: " + Title;
    }
}

public class Operaciones : MonoBehaviour
{

    public Text TextInformacion, TituloPrimero, TituloSegundo, TituloTercero;
    public Button ButtonMicrofono, ButtonOcultar;
    public Animator Animacion;

    public Image imageInformacion;

    public GameObject ResultadoPrimero, ResultadoSegundo, ResultadoTercero;

    private List<ItemLink> _listResultados = new List<ItemLink>();

    private System.Random _random = new System.Random();

    private int _restaFinalizar = 0;

    private int _recordingRoutine = 0;
    private string _microphoneID = null;
    private AudioClip _recording = null;
    private int _recordingBufferSize = 1;
    private int _recordingHZ = 22050;

    private SpeechToText _speechToText;
    private Assistant _service;

    private fsSerializer _serializer = new fsSerializer();

    private Dictionary<string, object> _context = null;

    private float wait;
    private bool check;

    private TextToSpeech _textToSpeech;
    private AudioClip _audioClip;
    private AudioSource _audioSrc;

    private string _TTS_content = "";
    public bool play = false;

    public bool Active
    {
        get { return _speechToText.IsListening; }
        set
        {
            if (value && !_speechToText.IsListening)
            {
                _speechToText.DetectSilence = true;
                _speechToText.EnableWordConfidence = true;
                _speechToText.EnableTimestamps = true;
                _speechToText.SilenceThreshold = 0.01f;
                _speechToText.MaxAlternatives = 0;
                _speechToText.EnableInterimResults = true;
                _speechToText.OnError = OnSTTError;
                _speechToText.InactivityTimeout = -1;
                _speechToText.ProfanityFilter = false;
                _speechToText.SmartFormatting = true;
                _speechToText.SpeakerLabels = false;
                _speechToText.WordAlternativesThreshold = null;
                _speechToText.StartListening(OnSTTRecognize, OnSTTRecognizeSpeaker);
            }
            else if (!value && _speechToText.IsListening)
            {
                _speechToText.StopListening();
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        LogSystem.InstallDefaultReactors();
        GameObject audioOject = new GameObject("AudioObject");

        _audioSrc = audioOject.AddComponent<AudioSource>();

        _service = new Assistant(Credenciales.ObtenerAsistente());
        _service.VersionDate = Credenciales.ASSISTANT_versionDate;

        _textToSpeech = new TextToSpeech(Credenciales.ObtenerTTS());

        _speechToText = new SpeechToText(Credenciales.ObtenerSTT());

        _speechToText.RecognizeModel = "es-ES_BroadbandModel";

        Dictionary<string, object> input = new Dictionary<string, object>();

        input.Add("text", "start conversation");

        MessageRequest messageRequest = new MessageRequest()
        {
            Input = input
        };

        _service.Message(OnMessage, OnFail, Credenciales.ASSISTANT_workSpaceID, messageRequest);

        ButtonOcultar.GetComponent<Button>().onClick.AddListener(delegate { ocultarInformacion(); });
    }

    // Update is called once per frame
    void Update()
    {
        if (play)
        {
            Debug.Log("Play on");
            play = false;
            Active = false;
            obtenerTTS();
            ButtonMicrofono.enabled = false;
            setColorButton(Color.gray);
        }

        if (check)
        {
            wait -= Time.deltaTime;
        }

        if ((wait < 0f) && (check))
        {
            check = false;
            ButtonMicrofono.enabled = true;
            setColorButton(Color.white);
        }
    }

    private void OnSTTError(string error)
    {
        Active = false;
        Log.Error("OnSTTError", "Error {0}", error);
    }

    private void OnSTTRecognize(SpeechRecognitionEvent result, Dictionary<string, object> customData)
    {
        if (result != null && result.results.Length > 0)
        {
            foreach (var res in result.results)
            {
                foreach (var alt in res.alternatives)
                {
                    TextInformacion.text = alt.transcript;

                    if (res.final)
                    {
                        string _conversationString = alt.transcript;
                        Log.Debug("STT.OnSTTRecognize()", _conversationString);

                        OnPointerUp(null);

                        Dictionary<string, object> input = new Dictionary<string, object>();

                        input["text"] = _conversationString;
                        MessageRequest messageRequest = new MessageRequest()
                        {
                            Input = input,
                            Context = _context
                        };
                        _service.Message(OnMessage, OnFail, Credenciales.ASSISTANT_workSpaceID, messageRequest);

                    }
                }

                if (res.keywords_result != null && res.keywords_result.keyword != null)
                {
                    foreach (var keyword in res.keywords_result.keyword)
                    {
                        Log.Debug("STT.OnSTTRecognize()", "keyword: {0}, confidence: {1}, start time: {2}, end time: {3}", keyword.normalized_text, keyword.confidence, keyword.start_time, keyword.end_time);
                    }
                }

            }
        }
    }

    private void OnSTTRecognizeSpeaker(SpeakerRecognitionEvent result, Dictionary<string, object> customData)
    {
        if (result != null)
        {
            foreach (SpeakerLabelsResult labelResult in result.speaker_labels)
            {
                Log.Debug("OnSTTRecognize()", string.Format("Resultado: {0} | Confiabilidad: {3}", labelResult.speaker, labelResult.from, labelResult.to, labelResult.confidence));
            }
        }
    }

    private void OnMessage(object response, Dictionary<string, object> customData)
    {
        Log.Debug("OnMessage()", "Response: {0}", customData["json"].ToString());

        fsData fsData = null;
        fsResult r = _serializer.TrySerialize(response.GetType(), response, out fsData);

        if (!r.Succeeded)
            throw new WatsonException(r.FormattedMessages);

        MessageResponse messageResponse = new MessageResponse();

        object obj = messageResponse;
        r = _serializer.TryDeserialize(fsData, obj.GetType(), ref obj);

        if (!r.Succeeded)
            throw new WatsonException(r.FormattedMessages);

        object tempContext = null;
        (response as Dictionary<string, object>).TryGetValue("context", out tempContext);

        if (tempContext != null)
            _context = tempContext as Dictionary<string, object>;
        else
            Log.Debug("Error", "Al obtener el context");

        System.Random rnd = new System.Random();

        object tempIntentsObj = null;
        (response as Dictionary<string, object>).TryGetValue("intents", out tempIntentsObj);

        if ((tempIntentsObj as List<object>).Count() > 1)
        {
            Animacion.GetComponent<Animator>();
            object tempIntents = ((tempIntentsObj as List<object>)[0]);

            object intents = null;
            //Parte de luis
            (tempIntents as Dictionary<string, object>).TryGetValue("intent", out intents);

            switch (intents.ToString())
            {
                case "PedirCosas":
                    Animacion.SetInteger("Random", rnd.Next(4));
                    break;
                default:
                    Animacion.SetInteger("Random", rnd.Next(4));
                    break;
            }
        }

        //Parte de luis

        object tempText = null;
        (messageResponse.Output as Dictionary<string, object>).TryGetValue("text", out tempText);
        object tempTextObj = (tempText as List<object>)[0];

        string output = tempTextObj.ToString();

        if (output != null)
        {
            string replaceActionTags = output.ToString();

            int position = replaceActionTags.IndexOf("<wait3>");

            if (position != -1)
                replaceActionTags = output.Replace("<wait3>", "<break time='3s'/>");

            position = replaceActionTags.IndexOf("<wait4>");

            if (position != -1)
                replaceActionTags = output.Replace("<wait4>", "<break time='4s'/>");

            position = replaceActionTags.IndexOf("<wait5>");

            if (position != -1)
                replaceActionTags = output.Replace("<wait5>", "<break time='5s'/>");

            output = replaceActionTags;
        }
        else
        {
            Log.Debug("Error", "Al extraer la salida");
        }

        if (output.Contains("<requestBook>") || output.Contains("<requestArticle>"))
        {
            String[] outputs = output.Split(new String[] { "<requestBook>" }, StringSplitOptions.None);
            String busqueda = outputs[1];
            String decir = outputs[0];
            _TTS_content = decir.Replace("<aux>", "");
            play = true;
            obtenerDatos(busqueda);
        }
        else
        {
            _TTS_content = output;
            play = true;
        }
    }

    private void OnFail(RESTConnector.Error error, Dictionary<string, object> customData)
    {
        Log.Error("OnFail", "Error {0}", error.ToString());
    }

    void ocultarInformacion()
    {
        if (TextInformacion.IsActive())
        {
            TextInformacion.gameObject.SetActive(false);
            imageInformacion.gameObject.SetActive(false);
        }
        else
        {
            TextInformacion.gameObject.SetActive(true);
            imageInformacion.gameObject.SetActive(true);
        }
    }

    private void obtenerTTS()
    {
        _textToSpeech.Voice = VoiceType.es_ES_Laura;
        _textToSpeech.ToSpeech(HandleToSpeechCallback, OnFail, _TTS_content, true);
    }

    private void HandleToSpeechCallback(AudioClip clip, Dictionary<string, object> customData)
    {
        if (Application.isPlaying && clip != null && _audioSrc != null)
        {
            _audioSrc.spatialBlend = 0.0f;
            _audioSrc.clip = clip;
            _audioSrc.Play();

            wait = clip.length;
            check = true;
        }
    }

    public void OnPointerDown(BaseEventData eventData)
    {
        Active = true;
        StartRecording();
    }

    public void OnPointerUp(BaseEventData eventData)
    {
        Invoke("NoMoreListen", 2f);
    }

    private void NoMoreListen()
    {
        Active = false;
        ButtonMicrofono.enabled = false;
        StopRecording();
    }

    private void StartRecording()
    {
        if (_recordingRoutine == 0)
        {
            UnityObjectUtil.StartDestroyQueue();
            _recordingRoutine = Runnable.Run(RecordingHandler());
        }
    }

    private void StopRecording()
    {
        if (_recordingRoutine != 0)
        {
            Microphone.End(_microphoneID);
            Runnable.Stop(_recordingRoutine);
            _recordingRoutine = 0;
        }
    }

    private IEnumerator RecordingHandler()
    {
        _recording = Microphone.Start(_microphoneID, true, _recordingBufferSize, _recordingHZ);
        yield return null;

        if (_recording == null)
        {
            StopRecording();
            yield break;
        }

        bool bFirstBlock = true;
        int midPoint = _recording.samples / 2;
        float[] samples = null;

        while (_recordingRoutine != 0 && _recording != null)
        {
            int writePos = Microphone.GetPosition(_microphoneID);
            if (writePos > _recording.samples || !Microphone.IsRecording(_microphoneID))
            {
                Log.Error("RecordingHandler()", "Microfono desconectado.");

                StopRecording();
                yield break;
            }

            if ((bFirstBlock && writePos >= midPoint)
                || (!bFirstBlock && writePos < midPoint))
            {
                samples = new float[midPoint];
                _recording.GetData(samples, bFirstBlock ? 0 : midPoint);

                AudioData record = new AudioData();
                record.MaxLevel = Mathf.Max(Mathf.Abs(Mathf.Min(samples)), Mathf.Max(samples));
                record.Clip = AudioClip.Create("Recording", midPoint, _recording.channels, _recordingHZ, false);
                record.Clip.SetData(samples, 0);

                _speechToText.OnListen(record);

                bFirstBlock = !bFirstBlock;
            }
            else
            {
                int remaining = bFirstBlock ? (midPoint - writePos) : (_recording.samples - writePos);
                float timeRemaining = (float)remaining / (float)_recordingHZ;

                yield return new WaitForSeconds(timeRemaining);
            }

        }

        yield break;
    }

    private void setColorButton(Color col)
    {
        var colors = ButtonMicrofono.GetComponent<Button>().colors;
        colors.normalColor = col;
        colors.highlightedColor = Color.green;
        colors.pressedColor = Color.magenta;
        ButtonMicrofono.GetComponent<Button>().colors = colors;
    }

    private void obtenerDatos(string output)
    {
        _listResultados.Clear();

        StartCoroutine(GetDataOpenLibra(output));
        output = output.Replace(" ", "+");
        StartCoroutine(GetDataBase(output));
        StartCoroutine(GetDataGredos(output));
        StartCoroutine(DisplayResults());
    }

    IEnumerator DisplayResults()
    {
        while (_restaFinalizar != 0)
            yield return new WaitForSeconds(0.4f);

        if (_listResultados.Count > 0)
        {
            string[] respuestasResultado =
            {
                "Tengo las siguientes recomendaciones",
                "Te presento los siguientes resultados",
                "Tengo lo siguiente",
                "Encontré esto",
                "Pude encontrar esto",
                "Aquí tienes",
                "Gracias por esperar, tengo lo siguente"
            };
            _TTS_content = respuestasResultado[_random.Next(0, respuestasResultado.Length)];
            play = true;

            Animacion.SetInteger("Random", _random.Next(5));

            var conFotos = _listResultados.Where(
                (r) => !string.IsNullOrEmpty(r.Imagen));

            if (conFotos.Count() > 0)
            {
                ItemLink seleccionFoto = conFotos.ElementAt(_random.Next(0, conFotos.Count()));

                if (ResultadoPrimero.GetComponent<Button>())
                    ResultadoPrimero.GetComponent<Button>().onClick.RemoveAllListeners();
                else
                    ResultadoPrimero.AddComponent<Button>();

                ResultadoPrimero.GetComponent<Button>().onClick.AddListener(delegate { OpenLink(seleccionFoto); });

                TituloPrimero.text = "";

                StartCoroutine(DownloadPhoto(seleccionFoto.Imagen, ResultadoPrimero.GetComponent<Image>()));
            }

            var sinFotos = _listResultados.Where(
                (r) => string.IsNullOrEmpty(r.Imagen));

            if (sinFotos.Count() > 0)
            {
                ItemLink sinFoto = sinFotos.ElementAt(_random.Next(0, sinFotos.Count()));

                if (ResultadoSegundo.GetComponent<Button>())
                    ResultadoSegundo.GetComponent<Button>().onClick.RemoveAllListeners();
                else
                    ResultadoSegundo.AddComponent<Button>();

                ResultadoSegundo.GetComponent<Button>().onClick.AddListener(delegate { OpenLink(sinFoto); });
                TituloSegundo.text = sinFoto.Title;

                ItemLink sinFoto2 = sinFotos.Where(
                    (r) => r.Title != sinFoto.Title).ElementAt(_random.Next(0, sinFotos.Count() - 1));

                sinFoto.Title = sinFoto.Title.Replace("&aacute;", "á");
                sinFoto.Title = sinFoto.Title.Replace("&eacute;", "é");
                sinFoto.Title = sinFoto.Title.Replace("&iacute;", "í");
                sinFoto.Title = sinFoto.Title.Replace("&oacute;", "ó");
                sinFoto.Title = sinFoto.Title.Replace("&uacute;", "ú");

                sinFoto2.Title = sinFoto2.Title.Replace("&aacute;", "á");
                sinFoto2.Title = sinFoto2.Title.Replace("&eacute;", "é");
                sinFoto2.Title = sinFoto2.Title.Replace("&iacute;", "í");
                sinFoto2.Title = sinFoto2.Title.Replace("&oacute;", "ó");
                sinFoto2.Title = sinFoto2.Title.Replace("&uacute;", "ú");

                if (ResultadoTercero.GetComponent<Button>())
                    ResultadoTercero.GetComponent<Button>().onClick.RemoveAllListeners();
                else
                    ResultadoTercero.AddComponent<Button>();

                ResultadoTercero.GetComponent<Button>().onClick.AddListener(delegate { OpenLink(sinFoto2); });
                TituloTercero.text = sinFoto2.Title;
            }
        }
        else
        {
            _TTS_content = "Lo siento, no pude encontrar lo que pidió";

            play = true;

            if (ResultadoPrimero.GetComponent<Button>())
            {
                ResultadoPrimero.GetComponent<Button>().onClick.RemoveAllListeners();
                TituloPrimero.text = "Bienvenido";
            }
            if (ResultadoSegundo.GetComponent<Button>())
            {
                ResultadoSegundo.GetComponent<Button>().onClick.RemoveAllListeners();
                TituloSegundo.text = "Bienvenido";
            }
            if (ResultadoTercero.GetComponent<Button>())
            {
                ResultadoTercero.GetComponent<Button>().onClick.RemoveAllListeners();
                TituloTercero.text = "Bienvenido";
            }
        }
    }

    void OpenLink(ItemLink item)
    {
        Application.OpenURL(item.Link);
    }

    IEnumerator DownloadPhoto(string url, Image imgDisplay)
    {
        var www = new WWW(url);
        yield return www;

        Texture2D texture = new Texture2D(www.texture.width, www.texture.height, TextureFormat.DXT1, false);

        www.LoadImageIntoTexture(texture);
        Rect rec = new Rect(0, 0, texture.width, texture.height);

        Sprite spriteToUse = Sprite.Create(texture, rec, new Vector2(0.5f, 0.5f), 100);

        imgDisplay.sprite = spriteToUse;

        www.Dispose();

        www = null;
    }

    public string DecodeHtmlChars(string source)
    {
        string[] parts = source.Split(new string[] { "&#x" }, StringSplitOptions.None);
        for (int i = 1; i < parts.Length; i++)
        {
            int n = parts[i].IndexOf(';');
            string number = parts[i].Substring(0, n);
            try
            {
                int unicode = Convert.ToInt32(number, 16);
                parts[i] = ((char)unicode) + parts[i].Substring(n + 1);
            }
            catch { }
        }
        return String.Join("", parts);
    }

    IEnumerator GetDataOpenLibra(string output)
    {
        _restaFinalizar++;
        string search = output.Replace("<requestBook>", "");

        string urlToSearch = "http://www.etnassoft.com/api/v1/get/?keyword=" + search;

        Debug.Log("RequestBook OpenLibra" + urlToSearch);

        UnityWebRequest newRequest = UnityWebRequest.Get(urlToSearch);

        yield return newRequest.SendWebRequest();

        if (newRequest.isNetworkError || newRequest.isHttpError)
        {
            Debug.Log(newRequest.error);
        }
        else
        {
            var resultSearch = JSON.Parse(newRequest.downloadHandler.text);

            if (resultSearch.Count > 0)
            {
                int max = 2;
                foreach (var result in resultSearch)
                {
                    if (max <= 0)
                        break;

                    _listResultados.Add(new ItemLink(result.Value["url_download"], result.Value["title"], result.Value["thumbnail"]));

                    max--;
                }
            }
            else
                Debug.Log("Error: OpenLibra");
        }
        _restaFinalizar--;
    }

    IEnumerator GetDataBase(string output)
    {
        _restaFinalizar++;
        string search = output.Replace("<requestBook>", "");

        string url = "https://www.base-search.net";
        string urlToSearch = url + "/Search/Results?filter[]=f_dclang%3A\"spa\"&lookfor=" + search + "&type=all&sort=score+desc%2Cid+asc&oaboost=1&refid=dcdden";
        Debug.Log("RequestBook Base" + urlToSearch);

        UnityWebRequest newRequest = UnityWebRequest.Get(urlToSearch);

        yield return newRequest.SendWebRequest();

        if (newRequest.isNetworkError || newRequest.isHttpError)
        {
            Debug.Log(newRequest.error);
        }
        else
        {
            Regex rgx = new Regex("<a target=\"_blank\" rel=\"noopener\" class=\"link-gruen bold\" href=\"(.*?)\".*?>(.+?)<\\/a>");
            MatchCollection matches = rgx.Matches(newRequest.downloadHandler.text);

            if (matches.Count > 0)
            {
                int max = 2;
                foreach (Match result in matches)
                {
                    if (max <= 0)
                        break;

                    _listResultados.Add(new ItemLink(result.Groups[1].Value, result.Groups[2].Value, ""));

                    max--;
                }
            }
            else
                Debug.Log("Error: Base");
        }
        _restaFinalizar--;
    }

    IEnumerator GetDataGredos(string output)
    {
        _restaFinalizar++;
        string search = output.Replace("<requestBook>", "");

        string url = "https://gredos.usal.es";
        string urlToSearch = url + "/jspui/simple-search?location=%2F&query=" + search + "&rpp=10&sort_by=score&order=desc";
        Debug.Log("RequestBook Gredos" + urlToSearch);

        UnityWebRequest newRequest = UnityWebRequest.Get(urlToSearch);

        yield return newRequest.SendWebRequest();

        if (newRequest.isNetworkError || newRequest.isHttpError)
        {
            Debug.Log(newRequest.error);
        }
        else
        {
            Regex rgx = new Regex("<a\\s(?:[^\\s>]*?\\s)*?href=\"\\/jspui\\/handle\\/(.*?)\".*?>(.+?)<\\/a>");
            MatchCollection matches = rgx.Matches(newRequest.downloadHandler.text);

            if (matches.Count > 0)
            {
                int max = 2;
                string text = "";

                foreach (Match result in matches)
                {
                    if (max <= 0)
                        break;

                    text = DecodeHtmlChars(result.Groups[2].Value);

                    if (text.Contains("Archivo Institucional") || text.Contains("Biblioteca Digital") || text.Contains("Repositorio Cientifico") || text.Contains("Repositorio Docente"))
                        continue;

                    _listResultados.Add(new ItemLink(url + "/jspui/handle/" + result.Groups[1].Value, text, ""));

                    max--;
                }
            }
            else
                Debug.Log("Error: Base");
        }
        _restaFinalizar--;
    }
}
