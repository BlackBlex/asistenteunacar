﻿using IBM.Watson.DeveloperCloud.Utilities;

public class Credenciales
{
    public static string STT_usuario = "6dd91796-ac92-4c28-8cc9-a4d89e6d6b83";
    public static string STT_contra = "hgi8ho62fLnc";
    public static string STT_url = "https://stream.watsonplatform.net/speech-to-text/api";
    public static string TTS_usuario = "c13f97f2-5b56-4792-9f3b-3bb5491d2a60";
    public static string TTS_contra = "gZPiz2KJZcAI";
    public static string TTS_url = "https://stream.watsonplatform.net/text-to-speech/api";
    public static string ASSISTANT_usuario = "11ffe551-8252-451a-a7db-8d3b6be7f048";
    public static string ASSISTANT_contra = "o8BYuK10PJfr";
    public static string ASSISTANT_url = "https://gateway.watsonplatform.net/assistant/api";
    public static string ASSISTANT_workSpaceID = "0a6bf378-4a9b-42d8-811d-f3d561ce802c";
    public static string ASSISTANT_versionDate = "2018-09-20"; //"2018-02-16";

    public static Credentials ObtenerAsistente()
    {
        return new Credentials(ASSISTANT_usuario, ASSISTANT_contra, ASSISTANT_url);
    }

    public static Credentials ObtenerTTS()
    {
        return new Credentials(TTS_usuario, TTS_contra, TTS_url);
    }

    public static Credentials ObtenerSTT()
    {
        return new Credentials(STT_usuario, STT_contra, STT_url);
    }
}
